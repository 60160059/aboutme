package buu.tinnapong.aboutme

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.databinding.DataBindingUtil
import buu.tinnapong.aboutme.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    lateinit var myButton: Button
    lateinit var editText: EditText
    lateinit var nicknameTextView: TextView

    private lateinit var binding: ActivityMainBinding
    private val myName: MyName = MyName("Tinnapong")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
//        setContentView(R.layout.activity_main)
        myButton = binding.doneButton
        editText = binding.nicknameEdit
        nicknameTextView = binding.nicknameText

        binding.apply {
            doneButton.setOnClickListener {
                addNickname(it)
            }

            nicknameText.setOnClickListener {
                updateNickname(it)
            }
            myName = this@MainActivity.myName
        }


//        myButton.setOnClickListener {v ->
//            addNickname(v)
//        }


//        nicknameTextView.setOnClickListener { v ->
//            updateNickname(v)
//        }

    }

    private fun addNickname(v: View) {
        binding.apply {
            myName?.nickname = editText.text.toString()
            doneButton.visibility = View.GONE
            nicknameEdit.visibility = View.GONE
            nicknameText.visibility = View.VISIBLE
            invalidateAll()


            val inputMethodManager = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            inputMethodManager.hideSoftInputFromWindow(v.windowToken, 0)
        }
    }

    private fun updateNickname (v: View) {
        binding.apply {
            nicknameEdit.visibility = View.VISIBLE
            doneButton.visibility = View.VISIBLE
            v.visibility = View.GONE
            nicknameEdit.requestFocus()
            val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.showSoftInput(binding.nicknameEdit, 0)
        }
    }
}